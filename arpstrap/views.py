from arpegio.blog import views

from .models import Post


class PostList(views.PostList):
    model = Post


class PostDetail(views.PostDetail):
    model = Post
