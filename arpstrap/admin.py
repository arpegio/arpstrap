from django.contrib import admin

from arpegio.tags.models import Tag
from arpegio.categories.models import Category

from .models import Post

admin.site.register(Tag)
admin.site.register(Category)
admin.site.register(Post)
