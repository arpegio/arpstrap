from django.db import models
from arpegio.blog.models import Post as arpegio_Post
from arpegio.blog.managers import PostManager
from arpegio.categories.models import Categorizable
from arpegio.tags.models import Taggable

class Post(arpegio_Post, Categorizable, Taggable):
    """The custom post model."""
    objects = PostManager()
