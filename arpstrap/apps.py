from django.apps import AppConfig


class ArpstrapConfig(AppConfig):
    name = 'arpstrap'
